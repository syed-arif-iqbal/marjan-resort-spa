<?php
$comments = '';
if(isset($_POST['comments']))
    $comments = htmlentities($_POST['comments'], ENT_COMPAT, 'UTF-8');

$_SESSION['book']['comments'] = $comments;

// echo '<pre>';
// print_r($_SESSION['book']);
?>

<div class="col-md-6">
        <fieldset class="mb20">
            <legend><?php echo $texts['BOOKING_DETAILS']; ?></legend>
            <div class="ctaBox">
                <div class="row">
                    <div class="col-md-6">
                        <p>
                            <?php
                            echo $texts['CHECK_IN'].' <strong>'.gmstrftime(DATE_FORMAT, $_SESSION['book']['from_date']).'</strong><br>
                            '.$texts['CHECK_OUT'].' <strong>'.gmstrftime(DATE_FORMAT, $_SESSION['book']['to_date']).'</strong><br>
                            <strong>'.$_SESSION['book']['nights'].'</strong> '.$texts['NIGHTS'].' -
                            <strong>'.($_SESSION['book']['adults']+$_SESSION['book']['children']).'</strong> '.$texts['PERSONS']; ?>
                        </p>
                    </div>
                </div>
            </div>
        </fieldset>
        <?php
        if(isset($_SESSION['book']['rooms']) && count($_SESSION['book']['rooms']) > 0){ ?>
            <fieldset class="mb20">
                <legend><?php echo ucfirst($texts['ROOMS']); ?></legend>
                <?php
                foreach($_SESSION['book']['rooms'] as $id_room => $rooms){
                    foreach($rooms as $index => $room){ ?>
                        <div class="row">
                            <div class="col-md-6">
                                <p>
                                    <?php
                                    echo '<strong>'.$room['title'].'</strong><br>
                                    '.($room['adults']+$room['children']).' '.$texts['PERSONS'].' - 
                                    '.$texts['ADULTS'].': '.$room['adults'].' / 
                                    '.$texts['CHILDREN'].': '.$room['children']; ?>
                                </p>
                            </div>
                            <div class="col-md-6">
                                <span class="pull-right">
                                    <?php echo formatPrice($room['amount']*CURRENCY_RATE); ?><br>
                                </span>
                            </div>
                        </div>
                        <?php
                    }
                } ?>
            </fieldset>
            <?php
        }
        if(isset($_SESSION['book']['activities']) && count($_SESSION['book']['activities']) > 0){ ?>
            <fieldset class="mb20">
                <legend><?php echo $texts['ACTIVITIES']; ?></legend>
                <?php
                $result_activity_file = $db->prepare('SELECT * FROM pm_activity_file WHERE id_item = :id_activity AND checked = 1 AND lang = '.LANG_ID.' AND type = \'image\' AND file != \'\' ORDER BY rank LIMIT 1');
                foreach($_SESSION['book']['activities'] as $id_activity => $activity){ 
                    
                    $result_activity_file->bindParam(':id_activity', $id_activity, PDO::PARAM_STR);
                    $result_activity_file->execute();?>
                    <div class="row">
                        <div class="col-md-6">
                            <p>
                                <?php
                                if($result_activity_file !== false && $db->last_row_count() > 0){
                                    $row = $result_activity_file->fetchAll(PDO::FETCH_ASSOC);
                                    $realpath = BASE_URL.'/medias/activity/medium/'. $row['id'] .'/'.$row['file'];
                                    echo '<img src="'.$realpath.'" class="img-fluid img-thumbnail">';
                                }
                                echo '<strong>'.$activity['title'].'</strong><br>' ?>
                                <?php /*
                                echo '<strong>'.$activity['title'].'</strong> - '.$activity['duration'].'<br>
                                <strong>'.gmstrftime(DATE_FORMAT.' '.TIME_FORMAT, $activity['session_date']).'</strong> -
                                <strong>'.($activity['adults']+$activity['children']).'</strong> '.$texts['PERSONS']; */ ?>
                            </p>
                        </div>
                        <div class="col-md-6">
                            <span class="pull-right">
                                <?php echo formatPrice($activity['amount']*CURRENCY_RATE); ?><br>
                            </span>
                        </div>
                    </div>
                    <?php
                } ?>
            </fieldset>
            <?php
        }
        $rooms_ids = array_keys($_SESSION['book']['rooms']);
        $query_service = 'SELECT * FROM pm_service WHERE';
        if(isset($_SESSION['book']['amount_rooms'])) $query_service .= ' rooms REGEXP \'[[:<:]]'.implode('|', $rooms_ids).'[[:>:]]\' AND';
        $query_service .= ' lang = '.LANG_ID.' AND checked = 1 ORDER BY rank';
        $result_service = $db->query($query_service);
        if($result_service !== false && $db->last_row_count() > 0){ ?>
            <fieldset class="mb20">
                <legend><?php echo $texts['EXTRA_SERVICES']; ?></legend>
                <?php
                foreach($result_service as $i => $row){
                    $id_service = $row['id'];
                    $service_title = $row['title'];
                    $service_descr = $row['descr'];
                    $service_long_descr = $row['long_descr'];
                    $service_price = $row['price'];
                    $service_type = $row['type'];
                    $service_rooms = explode(',', $row['rooms']);
                    $mandatory = $row['mandatory'];
                    
                    $nb_rooms = count(array_intersect($service_rooms, $rooms_ids));
                    if($nb_rooms == 0) $nb_rooms = 1;

                    $service_qty = 1;
                    if($service_type == 'person') $service_qty = $_SESSION['book']['adults']+$_SESSION['book']['children'];
                    if($service_type == 'adult') $service_qty = $_SESSION['book']['adults'];
                    if($service_type == 'child') $service_qty = $_SESSION['book']['children'];
                    if($service_type == 'person-night' || $service_type == 'qty-person-night') $service_qty = ($_SESSION['book']['adults']+$_SESSION['book']['children'])*$_SESSION['book']['nights'];
                    if($service_type == 'adult-night' || $service_type == 'qty-adult-night') $service_qty = $_SESSION['book']['adults']*$_SESSION['book']['nights'];
                    if($service_type == 'child-night' || $service_type == 'qty-child-night') $service_qty = $_SESSION['book']['children']*$_SESSION['book']['nights'];
                    if($service_type == 'qty-night' || $service_type == 'night') $service_qty = $_SESSION['book']['nights'];
                    if($service_type == 'night') $service_qty = $nb_rooms;
                    
                    $service_price *= $service_qty;

                    $service_selected = array_key_exists($id_service, $_SESSION['book']['extra_services']);
                    
                    if($mandatory == 1 && !$service_selected) $service_selected = true;

                    $checked = $service_selected ? ' checked="checked"' : ''; ?>

                    <div class="row form-group">
                        <label class="col-sm-<?php echo (strpos($service_type, 'qty') !== false) ? 7 : 10; ?> col-xs-9">
                            <input type="checkbox" name="extra_services[]" value="<?php echo $id_service; ?>" class="sendAjaxForm"<?php if($mandatory) echo ' disabled="disabled" data-sendOnload="1"'; ?> data-action="<?php echo getFromTemplate('common/update_booking.php'); ?>" data-target="#total_booking"<?php echo $checked;?>>
                            <?php
                            if($mandatory){ ?>
                                <input type="hidden" name="extra_services[]" value="<?php echo $id_service; ?>">
                                <?php
                            }
                            echo $service_title;
                            if($service_descr != ''){ ?>
                                <br><small><?php echo $service_descr; ?></small>
                                <?php
                            }
                            if($service_long_descr != ''){ ?>
                                <br><small><a href="#service_<?php echo $id_service; ?>" class="popup-modal"><?php echo $texts['READMORE']; ?></a></small>
                                <div id="service_<?php echo $id_service; ?>" class="white-popup-block mfp-hide">
                                    <?php echo $service_long_descr; ?>
                                </div>
                                <?php
                            } ?>
                        </label>
                        <?php
                        if(strpos($service_type, 'qty') !== false){
                            $qty = isset($_SESSION['book']['extra_services'][$id_service]['qty']) ? $_SESSION['book']['extra_services'][$id_service]['qty'] : 1; ?>
                            <div class="col-sm-3 col-xs-9">
                                <div class="input-group input-group-sm">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default btn-number" data-field="qty_service_<?php echo $id_service; ?>" data-type="minus" disabled="disabled" type="button">
                                            <i class="fas fa-fw fa-minus"></i>
                                        </button>
                                    </span>
                                    <input class="form-control input-number sendAjaxForm" type="text" max="20" min="1" value="<?php echo $qty; ?>" name="qty_service_<?php echo $id_service; ?>" data-action="<?php echo getFromTemplate('common/update_booking.php'); ?>" data-target="#total_booking">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default btn-number" data-field="qty_service_<?php echo $id_service; ?>" data-type="plus" type="button">
                                            <i class="fas fa-fw fa-plus"></i>
                                        </button>
                                    </span>
                                </div>
                            </div>
                            <?php
                        } ?>
                        <div class="col-sm-2 col-xs-3 text-right">
                            <?php
                            if(strpos($service_type, 'qty') !== false) echo 'x ';
                            echo formatPrice($service_price*CURRENCY_RATE); ?>
                        </div>
                    </div>
                    <?php
                } ?>
            </fieldset>
            <?php
        }
        if(isset($_SESSION['book']['amount_rooms'])){ /* ?>
            <fieldset class="mb20">
                <legend><?php echo $texts['DO_YOU_HAVE_A_COUPON']; ?></legend>
                <div class="form-group form-inline">
                    <input class="form-control" type="text" value="" name="coupon_code">
                    <a href="#" class="btn btn-primary sendAjaxForm" data-action="<?php echo getFromTemplate('common/update_booking.php'); ?>" data-target="#total_booking"><i class="fas fa-fw fa-check"></i></a>
                </div>
            </fieldset>
            <hr>
            <?php
            */
            /*if(ENABLE_TOURIST_TAX == 1 && isset($_SESSION['book']['tourist_tax'])){ ?>
                <div class="row">
                    <div class="col-md-12">
                        <p>
                            <strong><?php echo $texts['TOURIST_TAX']; ?></strong>
                            <span class="pull-right"><?php echo formatPrice($_SESSION['book']['tourist_tax']*CURRENCY_RATE); ?></span>
                        </p>
                    </div>
                </div>
                <?php
            }
            <hr>*/ ?>
            <div id="total_booking" class="mb15">
                <?php
                if(isset($_SESSION['book']['discount_amount']) && $_SESSION['book']['discount_amount'] > 0){ ?>
                    <div class="row">
                        <div class="col-xs-6 lead"><?php echo $texts['DISCOUNT']; ?></div>
                        <div class="col-xs-6 lead text-right"><?php echo '- '.formatPrice($_SESSION['book']['discount_amount']*CURRENCY_RATE); ?></div>
                    </div>
                    <?php
                } ?>
                <div class="row">
                    <div class="col-xs-6">
                        <h3>
                            <?php
                            echo $texts['TOTAL'].' <small>('.$texts['INCL_TAX'].')</small>'; ?>
                        </h3>
                    </div>
                    <div class="col-xs-6 lead text-right">
                        <?php echo formatPrice($_SESSION['book']['total']*CURRENCY_RATE); ?>
                    </div>
                </div>
                <?php
                $tax_id = 0;
                $result_tax = $db->prepare('SELECT * FROM pm_tax WHERE id = :tax_id AND checked = 1 AND value > 0 AND lang = '.LANG_ID.' ORDER BY rank');
                $result_tax->bindParam(':tax_id', $tax_id);
                foreach($_SESSION['book']['taxes'] as $tax_id => $taxes){
                    $tax_amount = 0;
                    foreach($taxes as $amount) $tax_amount += $amount;
                    if($tax_amount > 0){
                        if($result_tax->execute() !== false && $db->last_row_count() > 0){
                            $row = $result_tax->fetch(); ?>
                            <div class="row">
                                <div class="col-xs-6">
                                    <?php echo $row['name']; ?>
                                </div>
                                <div class="col-xs-6 text-right">
                                    <?php echo formatPrice($tax_amount*CURRENCY_RATE); ?>
                                </div>
                            </div>
                            <?php
                        }
                    }
                } ?>
            </div>
            <?php
        } ?>
        <fieldset>
            <legend><?php echo $texts['SPECIAL_REQUESTS']; ?></legend>
            <div class="form-group">
                <textarea class="form-control" name="comments" rows="5"><?php echo $comments; ?></textarea>
                <div class="field-notice" rel="comments"></div>
            </div>
            <p><?php //echo $texts['BOOKING_TERMS']; ?></p>
        </fieldset>
    </div>