<?php require_once("../../default/common.php"); 
?>

<script>
$(function(){
    $('[name*=activity_duration]').parents('.col-lg-6').prev().html('Normal Price&nbsp;<span class="red">*</span>');
    $('[name*=activity_lat_], [name*=activity_lng_]').val(1).parents('.row').hide();
    $('select[name*=activity_duration_unit] option:last, select[name*=activity_max_children] option:last, select[name*=activity_max_adults] option:last, select[name*=activity_max_people] option:last').prop('selected', true).parents('.row').hide();
})
</script>